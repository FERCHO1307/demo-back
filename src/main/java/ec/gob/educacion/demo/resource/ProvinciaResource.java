package ec.gob.educacion.demo.resource;

import ec.gob.educacion.demo.domain.Provincia;
import ec.gob.educacion.demo.service.ProvinciaService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/")
public class ProvinciaResource {

    private ProvinciaService provinciaService;

    public ProvinciaResource(ProvinciaService provinciaService) {
        this.provinciaService = provinciaService;
    }

    @RequestMapping
    public String getAllProvincias(Pageable pageable, Model model) {
        Page<Provincia> page = provinciaService.findAll(pageable);
        model.addAttribute("provincias", page.getContent());
        return "provincias";
    }

}
