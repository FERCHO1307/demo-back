package ec.gob.educacion.demo.service;

import ec.gob.educacion.demo.domain.Provincia;
import ec.gob.educacion.demo.repository.ProvinciaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProvinciaService {

    private final ProvinciaRepository provinciaRepository;

    public ProvinciaService(ProvinciaRepository provinciaRepository){
        this.provinciaRepository = provinciaRepository;
    }

    public Page<Provincia> findAll(Pageable pageable) {

        Page<Provincia> provincias = provinciaRepository.findAll(pageable);
        return provincias;
    }

    public List<Provincia> findAll() {

        List<Provincia> provincias = provinciaRepository.findAll();
        return provincias;
    }

}
