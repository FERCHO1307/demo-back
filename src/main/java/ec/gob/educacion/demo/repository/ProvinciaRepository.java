package ec.gob.educacion.demo.repository;

import ec.gob.educacion.demo.domain.Provincia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProvinciaRepository extends JpaRepository<Provincia, Long>{
}
